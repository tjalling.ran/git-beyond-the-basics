# Git Beyond The Basics

The presentation is hosted at http://tjalling.ran.gitlab.io/git-beyond-the-basics. [^1]

[^1]: This is an HTTP URL, not an HTTPS URL. Unfortunately, my account name `tjalling.ran` results in `tjalling.ran.gitlab.io`, which is considered a sub-subdomain of `gitlab.io`. `gitlab.io`'s HTTPS certificate is not valid for sub-subdomains, so we cannot use HTTPS.

Found by Riccardo, might be nice to incorporate in the future: https://initialcommit.com/tools/git-sim
